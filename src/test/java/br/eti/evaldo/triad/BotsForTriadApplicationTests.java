package br.eti.evaldo.triad;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * The Class BotsForTriadApplicationTests.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  03/08/2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableFeignClients
public class BotsForTriadApplicationTests {

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

}
