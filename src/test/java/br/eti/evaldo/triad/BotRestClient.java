package br.eti.evaldo.triad;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.eti.evaldo.triad.models.Bot;

/**
 * The Interface BotRestClient.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  03/08/2019
 */
//@FeignClient(url = "http://10.10.2.233:8080", name = "botRest")
@FeignClient(url = "http://localhost:8080/bots", name = "botRest")
public interface BotRestClient {

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	@GetMapping(value = "/{id}")
	Bot getById(@PathVariable("id") String id);

	@PostMapping(value = "/")
	Bot createBot(@RequestBody Bot bot);

	@GetMapping(value = "/lucas/")
	String getLucas();

}
