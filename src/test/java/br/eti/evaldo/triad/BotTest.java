package br.eti.evaldo.triad;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.test.context.junit4.SpringRunner;

import br.eti.evaldo.triad.models.Bot;

/**
 * The Class BotTest.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since 03/08/2019
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableFeignClients
public class BotTest {

	/** The http. */
	@Autowired
	private BotRestClient http;

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {

		Bot bot = new Bot();
		bot.setId("36b9f842-ee97-11e8-9443-0242ac120002");
		bot.setName("Aureo");

		createBot(bot);
		getBoot(bot.getId());

	}

	private void createBot(Bot bot) {
		Assert.assertEquals(bot, http.createBot(bot));
	}

	private void getBoot(String id) {

	}

}
