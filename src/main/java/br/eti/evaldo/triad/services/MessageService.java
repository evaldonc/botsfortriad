package br.eti.evaldo.triad.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.eti.evaldo.triad.models.Message;
import br.eti.evaldo.triad.repository.IMessageRepository;

/**
 * The Class MessageService.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  02/08/2019
 */
@Service
public class MessageService {

	/** The repository. */
	@Autowired
	private IMessageRepository repository;

	/**
	 * Creates the.
	 *
	 * @param message the message
	 * @return the message
	 */
	public Message create(Message message) {
		message.setId(UUID.randomUUID().toString());
		message = repository.save(message);
		return message;
	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	public Message getById(String id) {
		Optional<Message> obj = repository.findById(id);
		return obj.orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Object Not Found. " + id));
	}

	/**
	 * Gets the by conversation id.
	 *
	 * @param conversationId the conversation id
	 * @return the by conversation id
	 */
	public ResponseEntity<List<Message>> getByConversationId(String conversationId) {
		List<Message> list = repository.findByConversationId(conversationId);
		return ResponseEntity.ok(list);
	}


}
