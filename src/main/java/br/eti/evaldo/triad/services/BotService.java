package br.eti.evaldo.triad.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.eti.evaldo.triad.models.Bot;
import br.eti.evaldo.triad.repository.IBotRepository;

/**
 * The Class BotService.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  02/08/2019
 */
@Service
public class BotService {

	/** The repository. */
	@Autowired
	private IBotRepository repository;

	/**
	 * Creates the.
	 *
	 * @param bot the bot
	 * @return the bot
	 */
	public Bot create(Bot bot) {
		bot = repository.save(bot);
		return bot;
	}

	/**
	 * Gets the by id.
	 *
	 * @param id the id
	 * @return the by id
	 */
	public Bot getById(String id) {
		Optional<Bot> obj = repository.findById(id);
		return obj.orElseThrow(
				() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Object Not Found. " + id));
	}

	/**
	 * Update bot.
	 *
	 * @param bot the bot
	 * @return the bot
	 */
	public Bot updateBot(Bot bot) {
		bot = repository.save(bot);
		return bot;
	}

	/**
	 * Delete bot.
	 *
	 * @param id the id
	 */
	public void deleteBot(String id) {
		repository.deleteById(id);
	}

}
