package br.eti.evaldo.triad.dto;

import java.io.Serializable;

/**
 * The Class BotDTO.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  01/08/2019
 */
public class BotDTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8262309366759421653L;

	/** The id. */
	private String id;

	/** The name. */
	private String name;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}


}
