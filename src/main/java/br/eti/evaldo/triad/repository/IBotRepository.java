package br.eti.evaldo.triad.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.eti.evaldo.triad.models.Bot;

/**
 * The Interface IBotRepository.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  01/08/2019
 */
public interface IBotRepository extends JpaRepository<Bot, String> {

}
