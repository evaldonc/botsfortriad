package br.eti.evaldo.triad.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.eti.evaldo.triad.models.Message;

/**
 * The Interface IMessageRepository.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  02/08/2019
 */
public interface IMessageRepository extends JpaRepository<Message, String>{

	/**
	 * Find by conversation id.
	 *
	 * @param conversationId the conversation id
	 * @return the list
	 */
	List<Message> findByConversationId(String conversationId);

}
