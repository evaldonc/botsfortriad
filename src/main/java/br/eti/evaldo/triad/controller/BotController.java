package br.eti.evaldo.triad.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.eti.evaldo.triad.models.Bot;
import br.eti.evaldo.triad.services.BotService;

/**
 * The Class BotController.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  03/08/2019
 */
@RestController
@RequestMapping("/bots")
public class BotController {

	/** The service. */
	@Autowired
	private BotService service;

	/**
	 * Creates the bot.
	 *
	 * @param bot the bot
	 * @return the response entity
	 */
	@PostMapping
	public ResponseEntity<Bot> createBot(@RequestBody Bot bot) {
		bot = service.create(bot);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(bot.getId()).toUri();
		return ResponseEntity.created(uri).contentType(MediaType.APPLICATION_JSON).body(bot);
	}

	/**
	 * Gets the bot by id.
	 *
	 * @param id the id
	 * @return the bot by id
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Bot> getBotById(@PathVariable String id) {
		Bot bot = service.getById(id);
		return ResponseEntity.ok().body(bot);
	}

	/**
	 * Update bot.
	 *
	 * @param bot the bot
	 * @return the response entity
	 */
	@PutMapping
	public ResponseEntity<Bot> updateBot(@RequestBody Bot bot) {
		bot = service.updateBot(bot);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(bot.getId()).toUri();
		return ResponseEntity.created(uri).contentType(MediaType.APPLICATION_JSON).body(bot);
	}

	/**
	 * Delete bot.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteBot(@PathVariable String id) {
		service.deleteBot(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping(value = "/evaldo")
	public String evaldo() {
		return "Olá Lucas.";
	}


}
