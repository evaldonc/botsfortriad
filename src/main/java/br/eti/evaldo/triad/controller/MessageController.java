package br.eti.evaldo.triad.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.eti.evaldo.triad.models.Message;
import br.eti.evaldo.triad.services.MessageService;

/**
 * The Class MessageController.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  03/08/2019
 */
@RestController
@RequestMapping("/messages")
public class MessageController {

	/** The service. */
	@Autowired
	private MessageService service;

	/**
	 * Creates the message.
	 *
	 * @param message the message
	 * @return the response entity
	 */
	@PostMapping
	public ResponseEntity<Message> createMessage(@RequestBody Message message) {
		message = service.create(message);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(message.getId()).toUri();
		return ResponseEntity.created(uri).contentType(MediaType.APPLICATION_JSON).body(message);
	}

	/**
	 * Gets the message by id.
	 *
	 * @param id the id
	 * @return the message by id
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Message> getMessageById(@PathVariable String id) {
		Message message = service.getById(id);
		return ResponseEntity.ok().body(message);
	}

	/**
	 * Gets the message by conversation id.
	 *
	 * @param conversationId the conversation id
	 * @return the message by conversation id
	 */
	@GetMapping
	public ResponseEntity<List<Message>> getMessageByConversationId(@RequestParam String conversationId) {
		return service.getByConversationId(conversationId);
	}

}
