package br.eti.evaldo.triad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class BotsForTriadApplication.
 *
 * @author Evaldo Nascimento da Cruz (encruz)
 * @since  01/08/2019
 */
@SpringBootApplication
public class BotsForTriadApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(BotsForTriadApplication.class, args);
	}

}
